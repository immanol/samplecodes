import scala.sys.process._
import sbt.Keys._
import sbt._

logLevel := sbt.Level.Info

lazy val akkaVersion          = "2.5.13"
lazy val akkaHttpVersion      = "10.1.5"
lazy val alpakkaVersion       = "0.22"
lazy val apacheLoggingVersion = "2.11.1"
lazy val catsVersion          = "1.0.1"
lazy val cediConfigVersion    = "1.0.0"
lazy val circeVersion         = "0.9.3"
lazy val finagleVersion       = "18.2.0"
lazy val jacksonVersion       = "2.9.9"
lazy val kafkaVersion         = "1.1.1"
lazy val kamonCoreVersion     = "1.1.0"
lazy val kamonVersion         = "1.1.0"
lazy val logbackVersion       = "1.2.3"
lazy val nettyVersion         = "4.1.28.Final"
lazy val pactsVersion         = "2.3.3"
lazy val scalaCheckVersion    = "1.13.4"
lazy val scalaMetaVersion     = "4.0.0"
lazy val scalaMockVersion     = "4.1.0"
lazy val scalaTestVersion     = "3.0.4"
lazy val shapelessVersion     = "2.3.2"
lazy val tokenVersion         = "2.1.0"
lazy val toolkitSdkVersion    = "2.0.0"
lazy val toolkitVersion       = "2.1.0"
lazy val aspectJVersion       = "1.9.2"

val main_pwd: String = sys.env.getOrElse("MAIN_DECRYPT_PASSWORD", "dummy")

lazy val pwd: TaskKey[Unit] = taskKey[Unit]("decrypt passwords on server")

pwd := {
  s"./scripts/replacePasswords.sh $main_pwd" !
}

resolvers += Resolver.bintrayRepo("kamon-io", "sbt-plugins")
//resolvers += "confluent" at "https://packages.confluent.io/maven/"

addCompilerPlugin("org.spire-math" %% "kind-projector"   % "0.9.4")
addCompilerPlugin("org.scalameta"  % "semanticdb-scalac" % "4.0.0" cross CrossVersion.full)
addCompilerPlugin(scalafixSemanticdb)

scalafmtConfig in ThisBuild := file(".scalafmt.conf")

addCommandAlias("lint", "all compile:scalafix test:scalafix it:scalafix")

scalafmtOnCompile := true

compile := ((compile in Compile) dependsOn makePom).value

lazy val coreDependencies = Seq(
  "com.typesafe.akka"          %% "akka-actor"                                    % akkaVersion,
  "com.typesafe.akka"          %% "akka-http"                                     % akkaHttpVersion,
  "com.typesafe.akka"          %% "akka-slf4j"                                    % akkaVersion,
  "com.typesafe.akka"          %% "akka-stream"                                   % akkaVersion,
  "com.typesafe.akka"          %% "akka-stream-kafka"                             % alpakkaVersion,
  "com.lightbend.akka"         %% "akka-stream-alpakka-file"                      % "1.0-M1",
  "com.lightbend.akka"         %% "akka-stream-alpakka-csv"                       % "1.0-M1",
  "org.scalameta"              %% "scalameta"                                     % scalaMetaVersion,
  "org.scalameta"              %% "semanticdb"                                    % scalaMetaVersion,
  "org.apache.kafka"           % "kafka-clients"                                  % kafkaVersion,
  "ch.qos.logback"             % "logback-classic"                                % logbackVersion,
  "ch.qos.logback"             % "logback-core"                                   % logbackVersion,
  "org.codehaus.janino"        % "janino"                                         % "3.0.6",
  "io.spray"                   %% "spray-json"                                    % "1.3.4",
  "com.fasterxml.jackson.core" % "jackson-databind"                               % jacksonVersion,
  "org.apache.kafka"           %% "kafka"                                         % kafkaVersion % "test" exclude ("org.slf4j", "slf4j-log4j12"),
  "org.typelevel"              %% "cats-core"                                     % catsVersion,
  "io.kamon"                   %% "kamon-core"                                    % kamonCoreVersion,
  "io.kamon"                   %% "kamon-akka-2.5"                                % kamonVersion,
  "io.kamon"                   %% "kamon-prometheus"                              % kamonVersion,
  "io.kamon"                   %% "kamon-zipkin"                                  % "1.0.0",
  "io.kamon"                   %% "kamon-statsd"                                  % "1.0.0",
  "io.kamon"                   %% "kamon-system-metrics"                          % "1.0.0",
  "commons-daemon"             % "commons-daemon"                                 % "1.1.0",
  "io.confluent"               % "kafka-avro-serializer"                          % "3.3.1",
  "com.ccadllc.cedi"           %% "config"                                        % cediConfigVersion,
  "com.chuusai"                %% "shapeless"                                     % shapelessVersion,
  "com.twitter"                % "util-logging_2.12"                              % finagleVersion,
  "io.circe"                   %% "circe-parser"                                  % circeVersion,
  "io.circe"                   %% "circe-core"                                    % circeVersion,
  "io.circe"                   %% "circe-generic"                                 % circeVersion,
  "io.circe"                   %% "circe-jawn"                                    % circeVersion,
  "io.circe"                   %% "circe-optics"                                  % circeVersion,
  "org.julienrf"               % "enum_2.12"                                      % "3.1",
  "org.scalatest"              %% "scalatest"                                     % scalaTestVersion,
  "com.lightbend.akka"         %% "akka-stream-alpakka-file"                      % "1.0-M1",
  "com.twitter"                %% "bijection-avro"                                % "0.9.5",
  "org.aspectj"                % "aspectjweaver"                                  % aspectJVersion
)

dependencyOverrides += "com.fasterxml.jackson.core"   % "jackson-databind"             % jacksonVersion
dependencyOverrides += "com.fasterxml.jackson.module" % "jackson-module-scala_2.12"    % jacksonVersion
dependencyOverrides += "com.twitter"                  % "finagle-http_2.12"            % finagleVersion
dependencyOverrides += "com.twitter"                  % "finagle-http2_2.12"           % finagleVersion
dependencyOverrides += "com.twitter"                  % "finagle-netty4-http_2.12"     % finagleVersion
dependencyOverrides += "com.twitter"                  % "finagle-netty4_2.12"          % finagleVersion
dependencyOverrides += "com.twitter"                  % "finagle-base-http_2.12"       % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-collection_2.12"         % finagleVersion
dependencyOverrides += "com.twitter"                  % "finagle-core_2.12"            % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-tunable_2.12"            % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-security_2.12"           % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-jvm_2.12"                % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-hashing_2.12"            % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-codec_2.12"              % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-cache_2.12"              % finagleVersion
dependencyOverrides += "com.twitter"                  % "finagle-init_2.12"            % finagleVersion
dependencyOverrides += "com.twitter"                  % "finagle-toggle_2.12"          % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-logging_2.12"            % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-stats_2.12"              % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-lint_2.12"               % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-app_2.12"                % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-registry_2.12"           % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-core_2.12"               % finagleVersion
dependencyOverrides += "com.twitter"                  % "util-function_2.12"           % finagleVersion
dependencyOverrides += "io.netty"                     % "netty-codec-http"             % nettyVersion
dependencyOverrides += "io.netty"                     % "netty-transport-native-epoll" % nettyVersion
dependencyOverrides += "io.netty"                     % "netty-codec"                  % nettyVersion
dependencyOverrides += "io.netty"                     % "netty-buffer"                 % nettyVersion
dependencyOverrides += "io.netty"                     % "netty-common"                 % nettyVersion
dependencyOverrides += "io.netty"                     % "netty-handler"                % nettyVersion
dependencyOverrides += "io.netty"                     % "netty-transport"              % nettyVersion
dependencyOverrides += "org.aspectj"                  % "aspectjweaver"                % aspectJVersion

lazy val testDependencies = Seq(
  "com.typesafe.akka"   %% "akka-testkit"                       % akkaVersion,
  "com.typesafe.akka"   %% "akka-stream-testkit"                % akkaVersion,
  "net.manub"           %% "scalatest-embedded-kafka"           % kafkaVersion,
  "net.manub"           %% "scalatest-embedded-kafka-streams"   % kafkaVersion,
  "net.manub"           %% "scalatest-embedded-schema-registry" % kafkaVersion,
  "org.scalacheck"      %% "scalacheck"                         % scalaCheckVersion,
  "com.itv"             %% "scalapact-scalatest"                % pactsVersion,
  "com.itv"             %% "scalapact-circe-0-9"                % pactsVersion,
  "com.itv"             %% "scalapact-http4s-0-18"              % pactsVersion,
  "com.fortysevendeg"   %% "scalacheck-datetime"                % "0.2.0",
  "com.sksamuel.avro4s" %% "avro4s-core"                        % "2.0.1",
  "com.spotify"         %% "ratatool-scalacheck"                % "0.3.6",
  "com.google.guava"    % "guava"                               % "26.0-jre",
  "com.typesafe.akka"   %% "akka-stream-kafka-testkit"          % "1.0-RC1",
  "org.scalamock"       %% "scalamock"                          % scalaMockVersion
).map(_ % "test")

lazy val integrationTestDependencies = Seq(
  "org.scalatest"                 %% "scalatest" % scalaTestVersion,
  "tech.allegro.schema.json2avro" % "converter"  % "0.2.7"
).map(_ % "it,test")

fork in run := false

parallelExecution in Test := false
parallelExecution in ThisBuild := false

(sourceDirectory in AvroConfig) := new File("src/main/resources/avro")

lazy val ItTest = config("it") extend Test
javaOptions in ItTest += "-Xmx2G"

lazy val root = project
  .in(file("."))
  .configs(ItTest)
  .settings(inConfig(IntegrationTest)(scalafixConfigSettings(IntegrationTest)))
  .enablePlugins(BuildInfoPlugin,
                 JavaAppPackaging,
                 JavaAgent,
                 RpmPlugin,
                 RpmDeployPlugin,
                 SystemVPlugin)
  .settings(
    libraryDependencies ++= coreDependencies ++ testDependencies ++ integrationTestDependencies,
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "com.netNative"
  )
  .settings(
    javaAgents += "org.aspectj" % "aspectjweaver" % "1.9.2",
    javaOptions in Universal += "-Dorg.aspectj.tracing.factory=default"
  )
  .settings(mainClass in (Compile, run) := Some("com.netNative.accessprofiles.app.ProfileManagerService"))
  .settings(
    scalacOptions ++= List(
      "-deprecation:false",
      "-encoding",
      "UTF-8",
      "-target:jvm-1.8",
      "-language:existentials",
      "-language:higherKinds",
      "-language:postfixOps",
      "-unchecked",
      "-Yno-adapted-args",
      "-Ywarn-dead-code",
      "-Ywarn-inaccessible",
      "-Ywarn-unused",
      "-Ywarn-unused-import",
      "-language:implicitConversions",
      "-Yrangepos", // required by SemanticDB compiler plugin
      "-unchecked",
      "-deprecation",
      "-feature",
      "-Ypartial-unification",
      "-Ywarn-unused-import", // required by `RemoveUnused` rule
      "-language:postfixOps",
      "-P:semanticdb:synthetics:on",
      "-Xplugin-require:semanticdb"
    ),
    Compile / scalacOptions += {
      val t = crossTarget.value / "meta"
      s"-P:semanticdb:targetroot:$t"
    }
  )
  .settings(makePomConfiguration := makePomConfiguration.value.withFile(new File("pom.xml")))
  .settings(
    initialCommands in console :=
      """
        |import akka.stream._
        |import akka.actor._
      """.stripMargin
  )
  .settings(packageSettings: _*)
  .settings(Defaults.itSettings: _*)
  .settings(
    name := "profileManager",
    organization := "nl.netNative"
  )

lazy val profileSyncConf = project
  .in(file("target/conf"))
  .enablePlugins(RpmPlugin, RpmDeployPlugin)
  .settings(
    organization := "nl.netNative",
    name := "profilemanager-conf-".concat(buildEnv.value.toString.toLowerCase()),
    publishTo := artifactoryURL(isSnapshot.value),
    version := version.value.replace("-SNAPSHOT", ""),
    rpmVendor in Rpm := "netNative",
    rpmVendor := "netNative",
    rpmLicense := Some("Private"),
    // choose right resources
    unmanagedResourceDirectories in Compile += {
      file("src/main/resources-".concat(buildEnv.value.toString.toLowerCase))
    },
    // move resources to conf directory
    linuxPackageMappings in Rpm ++= {
      (resources in Compile).value
        .filterNot(file => file.isDirectory)
        .filter(file => file.name.endsWith("conf") || file.name.endsWith("xml"))
        .map(file =>
          packageMapping(
            (file, "/var/opt/pivotal/pivotal-tc-server-standard/profileManager/conf/" + file.getName)))
    }
  )
  .dependsOn(root)

fork in IntegrationTest := true
coverageEnabled in Test := true
