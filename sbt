#!/bin/bash
SBT_OPTS="-Dhttp.nonProxyHosts=*.netNative.net|*.europe.intranet -Xms1G -Xmx4G -Xss4M -XX:+CMSClassUnloadingEnabled -Dsbt.override.build.repos=true -Dsbt.repository.config=./repositories -Djavax.net.ssl.trustStore=./repositories-truststore.jks -Djavax.net.ssl.trustStorePassword=changeit -Dfile.encoding=utf-8 -Djsse.enableSNIExtension=false -Dapi-trust.log-ec-warning=false"
java $SBT_OPTS -jar `dirname $0`/sbt-launch.jar "$@"
