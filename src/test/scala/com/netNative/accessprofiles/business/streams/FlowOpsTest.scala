package com.netNative.accessprofiles.business.streams

import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.testkit.scaladsl.TestSink
import com.ing.accessprofiles.sync.models.{DataContainer, DataContextInstance}
import com.ing.heimdall.clients.models.AgreementUUID
import org.scalatest._

import scala.concurrent.Await
import scala.concurrent.duration._
class FlowOpsTest extends FlatSpec with EitherValues with Inside with Matchers {

  implicit val system                          = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()(system)

  behavior of "FlowOps.filterOutUnrelatedMingzFlow"

  it should "match AgreementUUID:3f3d8506-c5b6-4138-9d0c-743e98ad7155 with a pattern of range [0-3] " in {

    val agreementUUID = AgreementUUID(UUID.fromString("3f3d8506-c5b6-4138-9d0c-743e98ad7155"))
    val dataContainer1 = DataContainer(header = Map.empty[String, String],
                                       messageIds = Right(Seq(agreementUUID)),
                                       messageType = "message-type",
                                       topicName = "")

    val dataContextInstance1 = DataContextInstance(None, dataContainer1, Right(Seq.empty))
    val future = Source
      .single(dataContextInstance1)
      .via(FlowOps.filterOutUnrelatedMingzFlow("[0-3].*$"))
      .via(FlowOps.closeTracingFlow)
      .runWith(Sink.seq)

    val result = Await.result(future, 3.second)
    assert(result.nonEmpty)
    assert(result.map(_.data.messageIds) == Vector(dataContextInstance1).map(_.data.messageIds))
    assert(result.map(_.data.retryCount) == Vector(dataContextInstance1).map(_.data.retryCount + 1))
  }
  it should " not match AgreementUUID:3f3d8506-c5b6-4138-9d0c-743e98ad7155 with a pattern of range [e-f] " in {

    val agreementUUID = AgreementUUID(UUID.fromString("3f3d8506-c5b6-4138-9d0c-743e98ad7155"))
    val dataContainer1 = DataContainer(header = Map.empty[String, String],
                                       messageIds = Right(Seq(agreementUUID)),
                                       messageType = "message-type",
                                       topicName = "")

    val dataContextInstance1 = DataContextInstance(None, dataContainer1, Right(Seq.empty))
    val future = Source
      .single(dataContextInstance1)
      .via(FlowOps.filterOutUnrelatedMingzFlow("[e-f].*$"))
      .via(FlowOps.closeTracingFlow)
      .runWith(Sink.seq)

    val result = Await.result(future, 3.second)
    inside(result.flatMap(_.data.messageIds.right.value)) {
      case ids =>
        ids shouldBe empty
    }
  }

  it should "match AgreementUUID:3f3d8506-c5b6-4138-9d0c-743e98ad7155 and filterOut agreementUUID: a3a458a0-6592-492a-8a82-dd5431b4a6b1 with a pattern of range [0-3] " in {

    val correctAgreementUUID =
      AgreementUUID(UUID.fromString("3f3d8506-c5b6-4138-9d0c-743e98ad7155"))
    val inCorrectAgreementUUID =
      AgreementUUID(UUID.fromString("a3a458a0-6592-492a-8a82-dd5431b4a6b1"))
    val dataContainer1 = DataContainer(header = Map.empty[String, String],
                                       messageIds =
                                         Right(Seq(correctAgreementUUID, inCorrectAgreementUUID)),
                                       messageType = "message-type",
                                       topicName = "")
    val dataContainerExpected = DataContainer(header = Map.empty[String, String],
                                              messageIds = Right(Seq(correctAgreementUUID)),
                                              messageType = "message-type",
                                              topicName = "",
                                              retryCount = 1)

    val dataContextInstance1 = DataContextInstance(None, dataContainer1, Right(Seq.empty))
    val dataContextExpected  = DataContextInstance(None, dataContainerExpected, Right(Seq.empty))

    val sink = Source
      .single(dataContextInstance1)
      .via(FlowOps.filterOutUnrelatedMingzFlow("[0-3].*$"))
      .via(FlowOps.closeTracingFlow)
      .toMat(TestSink.probe[DataContextInstance])(Keep.right)
      .run()

    sink.request(1).expectNext(dataContextExpected)
  }

  // This test show that we did not implement some part of our graph properly. Some part of our code
  // have dependency. We show close INGTrace because is inside of DataContextInstance and also the hasAnythingToDo method is
  // private inside of SourceOps which is not god for testing
  it should "divert to unrelated sink with a pattern of range [e-f] " in {

    val correctAgreementUUID =
      AgreementUUID(UUID.fromString("3f3d8506-c5b6-4138-9d0c-743e98ad7155"))
    val inCorrectAgreementUUID =
      AgreementUUID(UUID.fromString("a3a458a0-6592-492a-8a82-dd5431b4a6b1"))
    val dataContainer1 = DataContainer(header = Map.empty[String, String],
                                       messageIds =
                                         Right(Seq(correctAgreementUUID, inCorrectAgreementUUID)),
                                       messageType = "message-type",
                                       topicName = "")
    val dataContainerExpected = DataContainer(header = Map.empty[String, String],
                                              messageIds = Right(Seq()),
                                              messageType = "message-type",
                                              topicName = "",
                                              retryCount = 1)

    val dataContextInstance1 = DataContextInstance(None, dataContainer1, Right(Seq.empty))
    val dataContextExpected  = DataContextInstance(None, dataContainerExpected, Right(Seq.empty))

    val (unrelatedSink, sink) = Source
      .single(dataContextInstance1)
      .via(FlowOps.filterOutUnrelatedMingzFlow("[e-f].*$"))
      .via(FlowOps.closeTracingFlow)
      .divertToMat(TestSink.probe[DataContextInstance], !SourceOps.hasAnythingToDo(_))(Keep.right)
      .toMat(TestSink.probe[DataContextInstance])(Keep.both)
      .run()

    unrelatedSink.request(1).expectNext(dataContextExpected)
    sink.request(1).expectComplete()
  }

}
