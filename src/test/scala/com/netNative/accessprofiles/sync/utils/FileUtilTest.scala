package com.netNative.accessprofiles.sync.utils

import com.ing.accessprofiles.sync.models.{DataContainer, DataContextInstance}
import com.ing.heimdall.clients.models.AgreementUUID
import org.scalatest.{FlatSpec, Matchers}

class FileUtilTest extends FlatSpec with Matchers with TestData with ServiceDiscoveryDisabler {

  behavior of "FileUtil"

  it should "return a list of 10000 partyId" in {

    FileUtil.onePamInitIds should have size 10000
  }

  it should "return true for UUID 0000a2f8-91d0-472d-ba0f-083051cb9c55" in {
    val dc = DataContainer(Map.empty[String, String],
                           Right(Seq(AgreementUUID(uuid("0000a2f8-91d0-472d-ba0f-083051cb9c55")))),
                           "",
                           "",
                           0,
                           None)
    val c = DataContextInstance(None, dc, Right(Seq.empty))
    val isContainsInOnePAMInitLoad: DataContextInstance => Boolean = (d: DataContextInstance) => {
      d.data.messageIds.map(_.exists(id => FileUtil.onePamInitIds.contains(id))).getOrElse(false)
    }
    isContainsInOnePAMInitLoad(c) should be(true)
  }

  it should "return false for UUID fd64f5e0-d3a1-4ee4-854f-80a25b756a07" in {
    val dc = DataContainer(Map.empty[String, String],
                           Right(Seq(AgreementUUID(uuid("fd64f5e0-d3a1-4ee4-854f-80a25b756a07")))),
                           "",
                           "",
                           0,
                           None)
    val c = DataContextInstance(None, dc, Right(Seq.empty))
    val isContainsInOnePAMInitLoad: DataContextInstance => Boolean = (d: DataContextInstance) => {
      d.data.messageIds.map(_.exists(id => FileUtil.onePamInitIds.contains(id))).getOrElse(false)
    }
    isContainsInOnePAMInitLoad(c) should be(false)
  }
}
