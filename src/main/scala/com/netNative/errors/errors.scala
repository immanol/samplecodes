package com.netNative

import com.netNative.accessprofiles.sync.logging.HeimdallLogger

package object errors {

  /**
    * Sugar for wrapped error types.
    * Most of the time we can just use [[AppError]] as error type.
    * Whenever we want a more specific type in a method signature we can use the other aliases
    */
  type AppError               = ErrorWrapper[_ <: ErrorInfo]
  type InternalErrorWrapped   = ErrorWrapper[_ <: InternalErrorInfo]
  type ConfigErrorWrapped     = ErrorWrapper[_ <: ConfigErrorInfo]
  type ParsingErrorWrapped    = ErrorWrapper[_ <: ParsingErrorInfo]
  type EncryptionErrorWrapped = ErrorWrapper[_ <: EncryptionErrorInfo]

  type ErrorOr[A] = Either[AppError, A]

  /**
    * Sugar for pattern matching when only the [[ErrorWrapper.errorInfo]] is needed
    */
  object Wrapped {
    def unapply[A <: ErrorInfo](wrappedError: ErrorWrapper[A]): Option[A] =
      Some(wrappedError.errorInfo)
  }

  object Presentation {
    def fatal(msg: String)(implicit logger: HeimdallLogger): Nothing = {
      logger.error(msg) // send to slf4j's log file
      sys.error(msg)    // abort execution
    }
  }
}
