package com.netNative.errors

import scala.reflect._

/**
  * Identifies "types" of error based on the scala [[Class]] and an optional [[ErrorVariation]].
  *
  * */
final case class ErrorType(runtimeClass: Class[_ <: ErrorInfo],
                           variation: Option[_ <: ErrorVariation]) {
  def isA[C <: ErrorInfo: ClassTag]: Boolean =
    classTag[C].runtimeClass isAssignableFrom runtimeClass
  def isA[C <: ErrorInfo: ClassTag](v: ErrorVariation): Boolean =
    isA[C] && variation.contains(v)
  override def toString: String = variation match {
    case Some(v) => s"${runtimeClass.getSimpleName}-$v"
    case None    => runtimeClass.getSimpleName
  }
}

object ErrorType {
  def apply[C <: ErrorInfo](errorInfo: C): ErrorType = errorInfo match {
    case r: RemoteServiceErrorInfo => ErrorType(errorInfo.getClass, Some(r.origin))
    case _                         => ErrorType(errorInfo.getClass, None)
  }
}
