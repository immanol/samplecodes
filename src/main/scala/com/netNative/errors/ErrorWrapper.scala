package com.netNative.errors

import com.netNative.heimdall.clients.ClientsConfig
import com.netNative.accessprofiles.sync.logging.{Encrypter, LogEncrypter}
import com.netNative.errors.errors.AppError

/**
  * Wraps [[com.netNative.errors.errors.AppError]] in an [[Exception]] to make it compatible with ???
  */
final case class AppException(e: AppError) extends Exception {
  override def getMessage: String = e.message
}

object ErrorWrapper {

  /*
    why: things that use ErrorWrapper shouldn't depend on the whole config (ApiServerConfig.config loads the whole config)
    example: if gatling calls util code from the app, which calls ErrorWrapper.apply, it will crash
             gatling doesn't have the same configs as the app
    idea: let ErrorWrapper be a pure model thing without encryption
          move encryption to a component responsible for creating log messages out of ErrorWrappers
          make this component a case class, so it's dependency on config is explicit in the parameters
          init this component from Server, the only place where configs are loaded
          maybe ErrorPresentation should become this component
   */
  val logEncryptionKey: String =
    ClientsConfig.config.getString("log-encryption-key")

  val encrypter: Encrypter = LogEncrypter(logEncryptionKey)

}

/**
  * Wraps [[ErrorInfo]] with additional info that any error can have.
  * Avoids repeated definition in subclasses of [[ErrorInfo]]
  *
  * @param errorInfo  Structured info specific to a type of error.
  *                   Example: Every [[HttpErrorReceived]] has an httpStatus, a responseBody and an origin.
  * @param extraText  Additional info in free text.
  * @param debugText  Additional info in free text that appear in logs but not in responses.
  * @param cause      Example: A [[???]] was caused by [[FailedToDecrypt]].
  *                   Can also wrap a [[Throwable]] (thrown by a library for example).
  */
final case class ErrorWrapper[A <: ErrorInfo](errorInfo: A,
                                              extraText: Option[String] = None,
                                              debugText: Option[String] = None,
                                              cause: Option[AppError] = None) {
  val timestamp: Long = System.currentTimeMillis

  def message: String =
    ErrorMessage(errorInfo).errorMessage.append(" ", extraText)

  def exception: Exception = AppException(this)

  def throwableCause: Option[Throwable] = cause.flatMap {
    _.errorInfo match {
      case JavaThrowable(t) => Some(t)
      case _                => None
    }
  }

  def withExtra(text: String): ErrorWrapper[A] = this.copy(extraText = Some(text))

  def withDebug(text: String): ErrorWrapper[A] = this.copy(debugText = Some(text))

  def withSensitive(text: String): ErrorWrapper[A] =
    this.copy(debugText = Some(ErrorWrapper.encrypter.encrypt(text)))

  def causedBy(cause: ErrorWrapper[_ <: ErrorInfo]): ErrorWrapper[A] =
    this.copy(cause = Some(cause))
  def causedBy(cause: Throwable): ErrorWrapper[A] = causedBy(JavaThrowable(cause))

  /**
    * Adds [[debugText]] from this error and the causes
    */
  private def aggregatedText: String = {
    message.append(" ", debugText).append(" Caused by: ", causeText)
  }

  private def causeText: Option[String] = cause.map { c =>
    c.errorInfo match {
      case JavaThrowable(t) => c.aggregatedText append (" At ", minimalStackTrace(t))
      case _                => c.aggregatedText
    }
  }

  /**
    * Selects from the stack trace the bottom most call coming from our application code.
    * [[ErrorInfo]]'s in general don't include a stack trace, only [[Throwable]]s do.
    */
  private def minimalStackTrace(t: Throwable): Option[String] =
    t.getStackTrace.find(_.getClassName.contains("com.netNative.permissions")).map(_.toString)

  private implicit class AppendOption(str: String) {
    def append(prefix: String, option: Option[String]): String =
      option match {
        case None        => str
        case Some(value) => s"$str$prefix$value"
      }
  }

}
