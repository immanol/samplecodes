package com.netNative.errors

/**
  * Translates an [[ErrorInfo]] to a user friendly message.
  */
final case class ErrorMessage(origin: String, errorCode: String, errorMessage: String)
object ErrorMessage {

  def apply(e: ErrorInfo): ErrorMessage = e match {
    case i: InternalErrorInfo if msgForInternalError.isDefinedAt(i) =>
      ErrorMessage("", s"${i.internalCode}", s"${msgForInternalError(i)}")
    case i: RemoteServiceErrorInfo if remoteServiceErrorInfo.isDefinedAt(i) =>
      remoteServiceErrorInfo(i)
    case _ => ErrorMessage("", "", e.toString)
  }

  private def msgForInternalError: PartialFunction[InternalErrorInfo, String] = {
    case ConfigMissingKey(file, key) =>
      s"File $file is missing $key key."
    case FailedToParseUUID(info)         => s"failed to parse UUID: $info"
    case FailedToParseKafkaMessage(info) => s"failed to parse Kafka message: $info"
    case JavaThrowable(t)                => s"${t.getMessage}"

    case _ => ""
  }

  private def remoteServiceErrorInfo: PartialFunction[RemoteServiceErrorInfo, ErrorMessage] = {
    case i: HttpErrorReceived =>
      ErrorMessage(
        i.origin.toString,
        errorCodeForRemoteServiceError(i),
        s"Call to ${i.origin} failed with code: ${i.receivedStatusCode} and body: ${i.receivedMessage
          .replaceAll("\n", "   ")}."
      )
    case i: MissingFieldInResponse =>
      ErrorMessage(i.origin.toString,
                   errorCodeForRemoteServiceError(i),
                   s"Call to ${i.origin} failed to provide field: ${i.fieldName}.")
    case i: ErrorReceived =>
      ErrorMessage(i.origin.toString,
                   errorCodeForRemoteServiceError(i),
                   s"Call to ${i.origin} returned an error.")
    case i: FailedToParseResponse =>
      ErrorMessage(i.origin.toString,
                   errorCodeForRemoteServiceError(i),
                   s"Call to ${i.origin} returned response which could not be parsed.")
    case i: ServerOverload =>
      ErrorMessage(i.origin.toString,
                   errorCodeForRemoteServiceError(i),
                   s"${i.origin} is having overload at the current moment")
  }

  private def errorCodeForRemoteServiceError: PartialFunction[RemoteServiceErrorInfo, String] = {
    case i: HttpErrorReceived => s"${i.receivedStatusCode}"
    case i: ErrorInfo         => s"${InternalCode(i)}"
    case _                    => ""
  }

}
