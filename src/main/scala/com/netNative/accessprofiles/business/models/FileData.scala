package com.netNative.accessprofiles.business.models

import java.util.UUID


case class FileData(mingzUUID: UUID, fileName: String, lineNumber: Int)
