package com.netNative.accessprofiles.business.streams

import com.netNative.accessprofiles.sync.flows.DataFlow
import com.netNative.accessprofiles.sync.logging.HeimdallLogger
import com.netNative.accessprofiles.sync.models.DataContextInstance
import com.netNative.accessprofiles.sync.utils.EncodersDecoders.KafkaNotificationDateDecoders
import com.netNative.errors.ErrorMessage
import org.slf4j.MDC

class LoggingFlow(name: String, logger: HeimdallLogger) extends DataFlow {

  override val flowName: String = name

  override def update(stream: DataContextInstance): DataContextInstance = {

    val hm = stream.data.header
    for {
      cid                 <- hm.get("ServiceEvent_CorrelationId")
      pid                 <- hm.get("ServiceEvent_ParentEventId")
      time                <- hm.get("NotificationTime")
      transactionType     <- hm.get("TransactionType")
      spanId              <- hm.get("SpanId")
      getNotificationTime <- hm.get("NotificationReceivedDateTime")
    } yield {
      MDC.put("traceId", cid)
      MDC.put("parentId", pid)
      MDC.put("requester", stream.dataWriter)
      MDC.put("spanId", spanId)
      MDC.put("timestamp",
              ProfileSyncTimeFormat
                .timeDiffInMilli(getNotificationTime)(KafkaNotificationDateDecoders.formatter)
                .toString)
      MDC.put("messages", s"$time | $transactionType")
    }
    stream.result match {
      case Left(e: AppError) => {
        MDC.put("errorCode", s"${ErrorMessage(e.errorInfo).errorCode}")
        MDC.put("errorMessage", s"${ErrorMessage(e.errorInfo).errorMessage}")
        MDC.put("errorTrail", s"${ErrorMessage(e.errorInfo).origin}")
        logger.error("NOK")
      }
      case Right(v) => {
        MDC.put("messages", s"${stream.data.messageType}")
        logger.debug(
          s"createData:${v.map(_.createResult.map(_.debugMessage))}~|~" +
            s"update:${v.map(_.updateResult.map(_.debugMessage))}~|~" +
            s"meansPatch:${v.map(_.meansPatchUpdateResult.map(_.debugMessage))}~|~" +
            s"meansUnPatch:${v.map(_.meansUnPatchUpdateResult.map(_.debugMessage))}~|~" +
            s"closeProfile:${v.map(_.profileCloseResult.map(_.debugMessage))}")
      }
    }
    MDC.clear()
    stream
  }

}
