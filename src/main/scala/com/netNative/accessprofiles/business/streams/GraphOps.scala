package com.netNative.accessprofiles.business.streams

import akka.stream._
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, MergePreferred, Partition, RunnableGraph}
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class GraphOps private (a2aBusiness: A2ABiz) {
  import GraphOps._

  def runGraph(
      config: Config): RunnableGraph[((Future[IOResult], Future[IOResult]), UniqueKillSwitch)] = {

    allGraphs(a2aBusiness)
  }
}

object GraphOps extends UnsafeLogger {

  def apply(a2aBusiness: Agreement2AgreementBiz, mandateBusiness: MandateBiz): GraphOps =
    new GraphOps(a2aBusiness, mandateBusiness)

  private val runA2ABusiness = (a2aBusiness: Agreement2AgreementBiz) =>
    (d: DataContextInstance) => {
      d.data.messageIds.map(ids => {
        a2aBusiness.run(ids)
      }) match {
        case Right(r) =>
          r.map(v => {
              d.span.finish()
              d.updateResult(v)
            })
            .asScala
        case Left(e) =>
          d.span.finish()
          Future(d.updateError(e))
      }
  }

  /**
    * a2aTopic ~> decodeA2A ~> accessLog ~> Merge.in0
    * mandateTopic ~> decodeMandate ~> accessLog ~> preProcess ~> Merge.in1
    * DirectoryChange ~> fileAdded ~> lineParser ~> accessLog ~> Merge.in2
    * Merge.out ~> runA2ABusiness ~> auditLog ~> CommitMessage ~> Sink.ignore
    *
    * @param a2aBusiness business implementation for a2a
    * @param mandateBusiness preprocessing for mandate
    * @param config configuration
    * @return
    */
  private def allGraphs(
      a2aBusiness: Agreement2AgreementBiz,
      config: Config): RunnableGraph[((Future[IOResult], Future[IOResult]), UniqueKillSwitch)] = {

    RunnableGraph.fromGraph(
      GraphDSL.create(
        SinkOps.succeedOutSink(ClientsConfig.appConfig.dedicatedSucceedPath),
        SinkOps.failedOutSink(ClientsConfig.appConfig.dedicatedFailedPath),
        KillSwitches.single[DataContextInstance]
      )((sf, ff, s) => ((sf, ff), s)) {
        implicit b => (succeededFileSink, failedFileSink, killSwitch) =>
          import GraphDSL.Implicits._

          val sourceMerge = b.add(MergePreferred[DataContextInstance](1))
          val statusPartition =
            b.add(new Partition[DataContextInstance](2, FlowOps.pathDetector, true))
          val succeedBroad = b.add(Broadcast[DataContextInstance](2, true))
          val outRetryPartition =
            b.add(
              new Partition[DataContextInstance](
                2,
                FlowOps.failedPathDetector(_)(
                  ClientsConfig.appConfig.graphSettings.failedRetryCount),
                false))

          SourceOps
            .combinedDedRelatedSources(config)(mPreProcess(mBusiness)) ~> sourceMerge ~>
            Flow[DataContextInstance]
              .mapAsyncUnordered(ClientsConfig.appConfig.graphSettings.dedicatedParallelismLevel)(
                runA2ABusiness(a2aBusiness)) ~> FlowOps.closeTracingFlow ~> FlowOps.accessLoggingFlow ~> statusPartition
          statusPartition ~> succeedBroad
          succeedBroad ~> succeededFileSink
          succeedBroad ~> killSwitch ~> SinkOps.relatedCommitSink
          statusPartition ~> FlowOps.updateFailedFlow ~> outRetryPartition
          outRetryPartition ~> failedFileSink
          sourceMerge.preferred <~ FlowOps.throttleFailedFlow <~ outRetryPartition

          ClosedShape
      })
  }
}
