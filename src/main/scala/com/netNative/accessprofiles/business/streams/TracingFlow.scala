package com.netNative.accessprofiles.business.streams

import com.netNative.accessprofiles.sync.flows.DataFlow
import com.netNative.accessprofiles.sync.models.DataContextInstance

class TracingFlow(name: String) extends DataFlow {

  override val flowName: String = name

  override def update(stream: DataContextInstance): DataContextInstance = {

    stream.span.finish()
    stream
  }

}
