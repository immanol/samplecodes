package com.netNative.accessprofiles.business.streams

import java.nio.file.{Path, StandardOpenOption}

import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink}
import akka.stream.IOResult
import akka.{Done, NotUsed}
import com.netNative.accessprofiles.sync.models.DataContextInstance

import scala.concurrent.Future

object SinkOps {

  val isSucceedMessage = (d: DataContextInstance) => d.result.isRight

  val unrelatedCommitSink: Sink[DataContextInstance, NotUsed] =
    Flow[DataContextInstance]
      .map(d => {
        d.span.finish()
        d.updateResult(Right(Seq.empty))

      })
      .via(FlowOps.commitFlow)
      .to(Sink.ignore)

  val relatedCommitSink: Sink[DataContextInstance, Future[Done]] =
    FlowOps.commitFlow
      .toMat(Sink.ignore)(Keep.right)

  val allOutSink: Path => Sink[DataContextInstance, Future[IOResult]] = (path: Path) =>
    Flow[DataContextInstance]
      .via(FlowOps.dataContextInstance2ByteString)
      .toMat(
        FileIO.toPath(
          path,
          Set(StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE)
        )
      )(Keep.right)

  val succeedOutSink: Path => Sink[DataContextInstance, Future[IOResult]] = (path: Path) =>
    Flow[DataContextInstance]
      .via(FlowOps.dataContextInstance2ByteString)
      .toMat(
        FileIO.toPath(
          path,
          Set(StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE)
        )
      )(Keep.right)

  val failedOutSink: Path => Sink[DataContextInstance, Future[IOResult]] = (path: Path) =>
    Flow[DataContextInstance]
      .via(FlowOps.dataContextInstance2ByteString)
      .toMat(
        FileIO.toPath(
          path,
          Set(StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE)
        )
      )(Keep.right)

}
