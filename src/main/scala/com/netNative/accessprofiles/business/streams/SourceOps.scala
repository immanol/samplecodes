package com.netNative.accessprofiles.business.streams

import java.nio.file.Path

import akka.NotUsed
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.alpakka.file.DirectoryChange
import akka.stream.alpakka.file.scaladsl.DirectoryChangesSource
import akka.stream.scaladsl.{FileIO, Flow, Framing, Merge, Source}
import akka.util.ByteString
import com.netNative.accessprofiles.sync.consumers.{
  AvroConsumerSettingsFactory,
  FileConsumer
}
import com.netNative.accessprofiles.sync.models.DataContextInstance
import com.netNative.accessprofiles.sync.models.sync.FileSourceTypes
import com.typesafe.config.Config

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Properties

object SourceOps {

  private val hasValidNotificationType =
    (filterTypes: Seq[String]) =>
      (d: DataContextInstance) =>
        d.data.messageIds match {
          case Right(_) =>
            filterTypes.contains(d.data.messageType)
          case Left(_) => false
    }

  private val isContainsInOnePAMInitLoad: DataContextInstance => Boolean =
    (d: DataContextInstance) => {
      d.data.messageIds.map(_.exists(id => FileUtil.InitIds.contains(id))).getOrElse(false)
    }
  private[streams] val hasAnythingToDo: DataContextInstance => Boolean =
    (d: DataContextInstance) => {
      d.data.messageIds match {
        case Left(_) => true // Strange!!! yes we want to log errors
        case Right(ids) =>
          ids.nonEmpty // nothing to process in this app instance, just go to the hell
      }
    }

  private val lineParser =
    (fileName: String) =>
      Framing
        .delimiter(
          delimiter = ByteString(Properties.lineSeparator),
          maximumFrameLength = 1024,
          allowTruncation = false
        )
        .map(_.utf8String)
        .recover {
          case _ => s"Problem in framing the file lines $fileName"
      }

  private def fileSource(isSourceEnabled: Boolean,
                         watchedFolder: Path,
                         fileSourceTypes: FileSourceTypes) = {
    if (isSourceEnabled) {
      DirectoryChangesSource(watchedFolder, pollInterval = 1.second, maxBufferSize = 1000)
        .via(Flow[(Path, DirectoryChange)].collect {
          case (p, change) if DirectoryChange.Creation == change => p
        })
        .via(
          Flow[Path]
            .flatMapConcat(
              path =>
                FileIO
                  .fromPath(path)
                  .via(lineParser(watchedFolder.getFileName.toString))
                  .filter(!_.isEmpty)
                  .map(s => (s, path.getFileName.toString))
                  .zipWithIndex
            )
        )
        .map(m => FileConsumer.decodeFileData(m._1._1, m._1._2, m._2, fileSourceTypes))
    } else {
      Source.empty[DataContextInstance]
    }
  }


  private val a2aSource =
    (settings: ConsumerSettings[String, Eventdata]) =>
      ClientsConfig.appConfig.graphSettings.isDedicatedKafkaSourceEnabled && ClientsConfig.appConfig.topics.agreementRelationshipTopic.isEnabled match {
        case false => Source.empty[DataContextInstance]
        case true => {
          Consumer
            .committableSource(
              settings,
              Subscriptions.topics(Set(A2ATopic.name.value))
            )
            .map(m => AvroConsumer.decodeAARelationship(m))
            .divertTo(SinkOps.unrelatedCommitSink,
                      !hasValidNotificationType(A2AtTopic.messageType)(_))
        }
    }


  /**
    * a2aTopic ~> decodeA2A ~> accessLog ~> Merge-0.in0
    * mandateTopic ~> decodeMandate ~> accessLog ~> preProcess ~> Merge-0.in1
    * Merge-0.out ~> Merge-1.in0
    * DirectoryChange ~> fileAdded ~> lineParser ~> accessLog ~> Merge-1.in2
    * Merge-1.out ~> ...
    */
  val combinedDedRelatedSources: Config => (
      DataContextInstance => Future[DataContextInstance]) => Source[DataContextInstance, NotUsed] =
    (config: Config) =>
      (mandatePreProcess: DataContextInstance => Future[DataContextInstance]) => {

        val a2aSettings =
          AvroConsumerSettingsFactory.consumerSettings(config, A2ATopic)

        val mandateSettings =
          AvroConsumerSettingsFactory.consumerSettings(config, MandateAgreementTopic)

        val parallelism = ClientsConfig.appConfig.graphSettings.dedicatedParallelismLevel

        val preProccessedMandateSource = mandateSource(mandateSettings)
          .mapAsync[DataContextInstance](parallelism)(p => mandatePreProcess(p))

        val kafkaSources = Source
          .combine(a2aSource(a2aSettings), preProccessedMandateSource)(Merge(_))
          .via(FlowOps.filterOutUnrelatedMingzFlow(ClientsConfig.appConfig.consumer.consumerFilter))
          .divertTo(SinkOps.unrelatedCommitSink, !hasAnythingToDo(_))

        val dedicatedFileSource =
          fileSource(ClientsConfig.appConfig.graphSettings.isDedicatedFileSourceEnabled,
                     ClientsConfig.appConfig.dedicatedInitLoadWatchedFolder,
                     FileSourceTypes.Agreement)

        Source
          .combine(kafkaSources, dedicatedFileSource)(Merge(_))
          .alsoTo(SinkOps.allOutSink(ClientsConfig.appConfig.dedicatedAllPath))
    }

}
