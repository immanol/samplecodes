package com.netNative.accessprofiles.business.streams

import java.time.ZonedDateTime
import java.util.concurrent.TimeUnit

import akka.kafka.ConsumerMessage.CommittableOffsetBatch
import akka.stream.scaladsl.Flow
import akka.util.ByteString
import akka.{Done, NotUsed}

import scala.concurrent.duration._

object FlowOps extends UnsafeLogger {

  val accessLoggingFlow: LoggingFlow =
    new LoggingFlow("accessloggingFlow_A2A", log)

  val closeTracingFlow: TracingFlow =
    new TracingFlow("tracingFlow_A2A")

  val dataContextInstance2ByteString: Flow[DataContextInstance, ByteString, NotUsed] =
    Flow[DataContextInstance].map(d => {

      val timestamp = ZonedDateTime
        .now()
        .format(KafkaNotificationDateDecoders.formatter)

      ByteString(s"$timestamp::${d.logSpanId()}::${d.data.retryCount}::${d.data.errorCode}\n")
    })

  val pathDetector = (d: DataContextInstance) =>
    d.result match {
      case Right(_) => 0
      case Left(_)  => 1
  }

  val failedPathDetector = (d: DataContextInstance) =>
    (retryCount: Int) => {
      d.result match {
        case Right(_) => 0
        case Left(_) =>
          d.data.errorCode match {
            case Some("500") => {
              if (d.data.retryCount >= retryCount) 0
              else 1
            }
            case _ => 0
          }
      }
  }

  val filterOutUnrelatedMingzFlow = (mingzFilter: String) =>
    Flow[DataContextInstance].map { d =>
      {
        d.updateData(d.data.messageIds.map(_.filter(_.value.toString matches mingzFilter)))
      }
  }

  val updateFailedFlow: Flow[DataContextInstance, DataContextInstance, NotUsed] =
    Flow[DataContextInstance].collect {
      case msg if msg.result.isLeft => msg.updateFailedData(msg.result)
    }

  val throttleFailedFlow = Flow[DataContextInstance]
    .throttle(
      1,
      FiniteDuration(ClientsConfig.appConfig.graphSettings.failedWaitTime, TimeUnit.SECONDS))

  val commitFlow: Flow[DataContextInstance, Done, NotUsed] = {
    Flow[DataContextInstance]
      .groupedWithin(ClientsConfig.appConfig.graphSettings.commitBatchCount,
                     ClientsConfig.appConfig.graphSettings.failedWaitTime.seconds)
      .map(group =>
        group.foldLeft(CommittableOffsetBatch.empty) { (batch, elem) =>
          {
            elem.span.finish()
            elem.message match {
              case Some(m) =>
                elem.result match {
                  case Right(_) => batch.updated(m.committableOffset)
                  case Left(_)  => CommittableOffsetBatch.empty
                }
              case None => CommittableOffsetBatch.empty
            }

          }
      })
      .mapAsync(4)(_.commitScaladsl())
  }

}
