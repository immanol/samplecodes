package com.netNative.accessprofiles.business

import cats.data.EitherT
import com.netNative.accessprofiles.sync.logging.UnsafeLogger
import com.netNative.errors.errors.AppError
import com.twitter.util.Future

class A2ABiz(pService: PService,
             mService: MService,
             iService: IService,
             aService: AService)
  extends UnsafeLogger {

  import A2ABiz._

  def run(aUUIDs: Seq[UUID]): Result[Seq[ProcessResult]] = {
    Future
      .collect(agreementUUIDs.map(aId => process(aId)))
      .map(flattenEither)
  }

  def process(fromUUID: tUUID): Result[ProcessResult] = {
    (for {
      data <- EitherT(fetchAndPartition(fromUUID))
      holder <- EitherT.fromEither[Future](data.mingzHolder)
      createResults <- EitherT(executeProfileCreate(data.createData, holder))
      updateResults <- EitherT(executeProfileUpdate(data.updateData))
      meansPatchResult <- EitherT(
        BizUtil.executeConnectProfileToMeans(mService)(data.updateData))
      inActivePatchResult <- EitherT(
        executeDisconnectProfileFromM(mService)(data.updateData))
      profileCloseResults <- EitherT(executeCloseProfile(data.updateData))
    } yield {
      ProcessResult(createResults,
        updateResults,
        meansPatchResult,
        inActivePatchResult,
        profileCloseResults)
    }).value

  }

  private[business] def fetchAndPartition(
                                           fromAgreementUUID: UUID): Result[AccessProfileFinalPicture] = {

    val ZSubscription: Result[ZSubscription] =
      resultMingz(pService)(fromAgreementUUID)

    val meansAgreements: ResultT[Seq[MAgreement]] = for {
      mingz <- EitherT(ZSubscription)
      means <- EitherT(getMeansAgreements(mService)(mingz.mAgreements))
    } yield means

    val inActiveMeansAgreements: ResultT[Seq[MAgreement]] = for {
      mingzs <- EitherT(ZSubscription) // MINGZ subscription
      inActiveMeans <- EitherT(
        getMAgreements(mService)(mingzs.inActiveMAgreements))
    } yield inActiveMeans.filterNot(_.isClosedAgreement)

    val accountsIPIPRs: ResultT[Seq[IPIPRelationship]] =
      for {
        mingz <- EitherT(ZSubscription)
        currentAccounts <- EitherT(getcurrentAccounts(PService)(mingz))
        accountHolders <- EitherT.fromEither(
          flattenEither(
            currentAccounts
              .filter(a => a.hasActiveHolder && !a.isClosedAgreement)
              .map(_.getActiveHolder))) // account holders
        ipiprs <- EitherT(getIpIpRelationshipsByPartyIds(iService)(accountHolders))
      } yield ipiprs

    val accessProfileIPIPRs
    : ResultT[Map[AccessProfile, Seq[IPIPRelationship]]] =
      for {
        mingz <- EitherT(ZSubscription)
        profiles <- EitherT(getAccessProfiles(AService)(mingz))
        ipiprIds <- EitherT(getIpIpIds(AService)(profiles))
        ipiprs <- EitherT(getIpIpRelationships(iService)(ipiprIds))
      } yield ipiprs

    val resultDiff = for {
      mingz <- EitherT(ZSubscription) // Ming subscription
      m <- meansAgreements
      inactiveMeans <- inActiveMeansAgreements
      grantorGranteeIPIPRs <- accountsIPIPRs
      meansToIPIPRs = diffRepresentativesAndMeansOwner(grantorGranteeIPIPRs.distinct, m)
      _ = log.debug(s"Mingz: ${fromAgreementUUID.value} => diff:$meansToIPIPRs")
      profileToIPIPRs <- accessProfileIPIPRs
    } yield partitionDiff(meansToIPIPRs, profileToIPIPRs, mingz, inactiveMeans)

    resultDiff.value

  }

  private def executeCloseProfile(updateData: Map[ProfileUUID, AccessProfileModificationData])
  : Result[Seq[UpdateProfileResult]] = ???

  private def executeProfileUpdate(updateData: Map[ProfileUUID, AccessProfileModificationData])
  : Result[Seq[UpdateProfileResult]] = ???

  /**
    * This is used in test also so it is public
    *
    * @param means2IPIRs m to IPIPRelationships to create new AccessProfile
    * @param mingzHolder holder organization of MINGZ subscription
    * @return
    */
  private def executeProfileCreate(
                                    means2IPIRs: Map[Seq[MAgreement], Seq[IPIPRelationship]],
                                    mingzHolder: PartyUUID): Future[Either[AppError, Seq[CreateProfileResult]]] = ???
}

object A2ABiz {

  def diffRepresentativesAndMeansOwner(
                                        currentAccountHolderIPIPRs: Seq[IPIPRelationship],
                                        meansAgreements: Seq[MAgreement])
  : Map[Seq[MAgreement], Seq[IPIPRelationship]] = {

    meansAgreements
      .groupBy(_.getActiveHolder)
      .map {
        case (meansHolderUUID, m) => {
          val ipiprs = currentAccountHolderIPIPRs.filter(
            _.grantee match {
              case Some(representative) => meansHolderUUID.contains(representative.id)
              case _ => false
            }
          )
          m -> ipiprs
        }
      }
  }

  def partitionDiff(
                     meansIPIPRMapping: Map[Seq[MAgreement], Seq[IPIPRelationship]],
                     profileIPIPRMapping: Map[AccessProfile, Seq[IPIPRelationship]],
                     ZSubscription: ZSubscription,
                     inActiveMeansAgreements: Seq[MAgreement]): AccessProfileFinalPicture = ???

  private def insertPartitionDiff(
                                   meansIPIPRMapping: Map[Seq[MAgreement], Seq[IPIPRelationship]],
                                   updatedIpIps: Seq[IPIPRelationship])
  : Map[Seq[MAgreement], Seq[IPIPRelationship]] = ???

  private def updatePartitionDiff(
                                   meansIPIPRMapping: Map[Seq[MAgreement], Seq[IPIPRelationship]],
                                   profileIPIPRMapping: Map[AccessProfile, Seq[IPIPRelationship]],
                                   inActiveMeansAgreements: Seq[MAgreement])
  : Map[ProfileUUID, AccessProfileModificationData] = {

    val allIPIPRsOfMeans: Seq[IPIPRelationship] =
      meansIPIPRMapping.values.toSeq.flatten

    profileIPIPRMapping
      .map { profile2IPIPR => {
        val profileUUIUD: ProfileUUID = profile2IPIPR._1.id
        val accessProfileUser: Option[PartyUUID] = profile2IPIPR._1.profileUser
        val allIPIPRsOfProfile: Seq[IPIPRelationship] = profile2IPIPR._2

        val ipipsToBeRemoved = allIPIPRsOfProfile.diff(allIPIPRsOfMeans)

        val ipipsToBeAdded = allIPIPRsOfMeans
          .diff(allIPIPRsOfProfile)
          .filter(ipip => matchesGrantee(accessProfileUser, ipip))

        val meansShouldBePatch: Seq[MAgreement] = meansIPIPRMapping
          .collect {
            case (m, ipips)
              if ipips.exists(ipip => matchesGrantee(accessProfileUser, ipip)) =>
              m.filterNot(_.accessProfiles.hasId(profileUUIUD))
          }
          .flatten
          .toSeq

        val meansShouldBeUnPatch: Seq[MAgreement] = inActiveMeansAgreements.collect {
          case m if m.accessProfiles.hasId(profileUUIUD) => m
        }

        val profileShouldBeClosed = !meansIPIPRMapping.keys.flatten.toSeq.exists(
          m =>
            (for {
              meansHolder <- m.getActiveHolder.toOption
              accprfUsser <- accessProfileUser
            } yield meansHolder == accprfUsser).getOrElse(false)
        )

        profileUUIUD -> AccessProfileModificationData(ipipsToBeRemoved,
          ipipsToBeAdded,
          meansShouldBePatch,
          meansShouldBeUnPatch,
          profileShouldBeClosed).checkClose
      }
      }
      .filterNot {
        case (_, apmd) => apmd.isEmpty
      }
  }

  val resultMingz: PService => UUID => Result[ZSubscription] =
    (PService: PService) =>
      (fromAgreementUUID: UUID) =>
        PService.getZSubscription(fromUUID)

  val getMs
  : mService => Seq[AgreementRelationship] => Result[Seq[MAgreement]] =
    (mService: mService) =>
      (MAgreement: Seq[AgreementRelationship]) =>
        Future
          .collect(
            MAgreement.map(ma => mService.getMeansAgreement(ma.agreement.id)))
          .map(flattenEither)

  val getcurrentAccounts
  : PService => ZSubscription => Result[Seq[ProductAgreement]] =
    (PService: PService) =>
      (mingz: ZSubscription) =>
        Future
          .collect(mingz.accountAgreements.map(pa =>
            PService.getAgreement(pa.agreement.id)))
          .map(flattenEither)

  val getIpIpRelationshipsByPartyIds: IService => Seq[PartyUUID] => Result[
    Seq[IPIPRelationship]] =
    (iService: IService) =>
      (partyIds: Seq[PartyUUID]) =>
        Future
          .collect(
            partyIds.map(id =>
              iService.getIPIPRelationshipsByParty(id))
          )
          .map(flattenSeqEitherOfSeq)

  val getAccessProfiles: AService => ZSubscription => Result[Seq[AccessProfile]] =
    (AService: AService) =>
      (mingz: ZSubscription) =>
        mingz.getHolder
          .map(partyId => AService.getAccessProfileIDsByInvolvedParty(partyId))
          .getOrElse(Future.value(Right(Seq.empty[AccessProfile])))

  val getIpIpIds: AService => Seq[AccessProfile] => Result[
    Map[AccessProfile, Seq[RelationshipUUID]]] =
    (AService: AService) =>
      (profiles: Seq[AccessProfile]) =>
        Future
          .collect(
            profiles
              .map(ap => (ap -> AService.getAccessProfileIPIPRelationships(ap.id)))
              .toMap
          )
          .map(flattenMapEither)

  val getIpIpRelationships
  : iService => Map[AccessProfile, Seq[RelationshipUUID]] => Result[
    Map[AccessProfile, Seq[IPIPRelationship]]] =
    (iService: iService) =>
      (profileRelationIds: Map[AccessProfile, Seq[RelationshipUUID]]) => {

        val x: Map[AccessProfile,
          Future[Either[AppError, Seq[IPIPRelationship]]]] =
          profileRelationIds.map {
            case (profile, relationIds) =>
              profile ->
                Future
                  .collect(
                    relationIds.map(relIp =>
                      iService.getIPIPRelationship(relIp))
                  )
                  .map(flattenEither)
          }

        Future.collect(x).map(flattenMapEither)
      }

  private def matchesGrantee(profileUserId: Option[PartyUUID],
                             meansIPIPR: IPIPRelationship) = {
    profileUserId == meansIPIPR.grantee.map(_.id)
  }

}
