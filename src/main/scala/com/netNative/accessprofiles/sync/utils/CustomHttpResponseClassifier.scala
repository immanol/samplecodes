package com.netNative.accessprofiles.sync.utils

import com.twitter.finagle.http.filter.HttpNackFilter.RetryableNackHeader
import com.twitter.finagle.http.{Response, Status}
import com.twitter.finagle.service.{ReqRep, ResponseClass, ResponseClassifier}
import com.twitter.util.Return

object CustomHttpResponseClassifier {

  def convertTo500Response(r: Response): Boolean =
    r.statusCode == 404 && r.contentString.contains("reset by peer")

  def is500(r: Response): Boolean =
    (r.statusCode >= 500 && r.statusCode <= 599) || convertTo500Response(r)

  private[this] def isRetryableNack(rep: Response): Boolean =
    rep.status == Status.ServiceUnavailable && rep.headerMap.contains(RetryableNackHeader)

  /**
    * Categorizes responses with status codes in the 500s as
    * [[ResponseClass.NonRetryableFailure NonRetryableFailures]].
    *
    * @note that retryable nacks are categorized as a [[ResponseClass.RetryableFailure]].
    */
  val ServerErrorsAsFailures: ResponseClassifier =
    ResponseClassifier.named("ServerErrorsAsFailures") {
      case ReqRep(_, Return(r: Response)) if is500(r) =>
        if (isRetryableNack(r))
          ResponseClass.RetryableFailure
        else
          ResponseClass.NonRetryableFailure
    }
}
