package com.netNative.accessprofiles.sync.utils

import enum.Enum
import io.circe.generic.AutoDerivation
import io.circe.syntax._
import io.circe.{Decoder, Encoder, Json}
import java.time.{ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter

import spray.json.{DeserializationException, JsString, JsValue, RootJsonFormat}

import scala.util.{Failure, Success, Try}

/**
  * Provides implicit Encoders and Decoders for:
  *   - any value class
  *   - any case class (inherits {@link io.circe.generic.auto}'s behavior)
  *   - types we can't create a companion object for: java libs
  */
object EncodersDecoders extends AutoDerivation {

  import shapeless._

  implicit def encoderValueClass[T <: AnyVal, V](implicit g: Lazy[Generic.Aux[T, V :: HNil]],
                                                 e: Encoder[V]): Encoder[T] = Encoder.instance {
    value ⇒
      e(g.value.to(value).head)
  }

  implicit def decoderValueClass[T <: AnyVal, V](implicit g: Lazy[Generic.Aux[T, V :: HNil]],
                                                 d: Decoder[V]): Decoder[T] = {
    Decoder.instance { cursor ⇒
      d(cursor) match {
        case Left(decodingFailure) => Left(decodingFailure)
        case Right(value)          => Right(g.value.from(value :: HNil))
      }
    }
  }


  def encodeEnum[A](field: String): Encoder[A] = Encoder.instance { e =>
    Json.obj(
      field -> e.toString.asJson
    )
  }

  def encodeEnum[A]: Encoder[A] = Encoder.instance { e =>
    e.toString.asJson
  }

  def decodeEnum[A](field: String)(implicit evidence: Enum[A]): Decoder[A] =
    Decoder.instance(c =>
      c.downField(field).as[String] match {
        case Left(a) => Left(a)
        case Right(enum) =>
          evidence
            .decode(enum)
            .left
            .map(df => io.circe.DecodingFailure(s"$enum is not one of ${df.validValues}", List()))
    })

  def decodeEnum[A](implicit evidence: Enum[A]): Decoder[A] =
    Decoder.instance(c =>
      c.as[String] match {
        case Left(a) => Left(a)
        case Right(enum) =>
          evidence
            .decode(enum)
            .left
            .map(df => io.circe.DecodingFailure(s"$enum is not one of ${df.validValues}", List()))
    })

  /*
    Encodes response and filters json on fields supplied
   */

  /*
    Encodes response and filters json on fields supplied
   */

  object DateDecoders {

    lazy val formatter: DateTimeFormatter = DateTimeFormatter
      .ofPattern("yyyy-MM-dd HH:mm:ss.Sz")
      .withZone(ZoneId.of("Z")) //2017-12-19T23:00:00.0Z

    implicit val timeEnc: Encoder[ZonedDateTime] =
      Encoder.encodeString.contramap(_.format(formatter))
    implicit val timeDec: Decoder[ZonedDateTime] =
      Decoder.decodeString.map(d => ZonedDateTime.parse(d, formatter))

    implicit val formatDateTime: RootJsonFormat[ZonedDateTime] = new RootJsonFormat[ZonedDateTime] {
      override def write(obj: ZonedDateTime): JsValue = JsString(obj.format(formatter))

      override def read(json: JsValue): ZonedDateTime = json match {
        case JsString(value) => ZonedDateTime.parse(value, formatter)
        case e               => throw new DeserializationException(s"Error in parsing DateTime:${e.toString}")
      }
    }

  }

  object KafkaNotificationDateDecoders {

    lazy val formatter: DateTimeFormatter =
      DateTimeFormatter
        .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz")
        .withZone(ZoneId.of("Z")) //2018-10-11T05:21:44.519Z

  }

}
