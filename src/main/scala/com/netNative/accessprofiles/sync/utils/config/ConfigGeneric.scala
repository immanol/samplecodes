package com.netNative.accessprofiles.sync.utils.config

import com.ccadllc.cedi.config.{ConfigErrors, ConfigParser}
import com.netNative.accessprofiles.sync.utils.config.ConfigGeneric.Password
import com.twitter.util.Duration
import com.netNative.accessprofiles.sync.logging.UnsafeLogger

import scala.util.Try

trait ConfigGeneric extends UnsafeLogger {

  def validateConfig[A](confs: Either[ConfigErrors, A]): A =
    confs match {
      case Left(errors) => log.fatal(s"Config validation failed: $errors")
      case Right(c)     => c
    }

  val usernameParser: ConfigParser[String] = ConfigParser.string("username")

  def parsePassword(key: String): ConfigParser[Password] =
    ConfigParser.fromString(key) { value =>
      Right(value.toCharArray)
    }
  val passwordParser: ConfigParser.DerivedConfigFieldParser[Password] =
    ConfigParser.DerivedConfigFieldParser(parsePassword)

  def parseDuration(key: String): ConfigParser[Duration] = ConfigParser.fromString(key) { value =>
    Try {
      val dur = scala.concurrent.duration.Duration(value)
      Duration(dur.length, dur.unit)
    }.recover {
        case _ =>
          val millis = value.toLong
          require(millis >= 0)
          Duration.fromMilliseconds(millis)
      }
      .toOption
      .toRight("Couldn't parse duration. Valid examples are '5' and '5 seconds'")
  }
  implicit val durationParser: ConfigParser.DerivedConfigFieldParser[Duration] =
    ConfigParser.DerivedConfigFieldParser(parseDuration)

  /**
    * T: Target Type
    */
  def valueClassStringParser[T](f: String => T): ConfigParser.DerivedConfigFieldParser[T] =
    ConfigParser.DerivedConfigFieldParser { key =>
      ConfigParser.fromString(key) { value =>
        Right(f(value))
      }
    }

}

object ConfigGeneric {
  type Password = Array[Char]
}
