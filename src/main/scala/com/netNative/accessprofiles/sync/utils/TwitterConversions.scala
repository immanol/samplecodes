package com.netNative.accessprofiles.sync.utils

import java.util.concurrent.TimeUnit.MILLISECONDS

import com.twitter.util.{Return, Throw, Duration => TwitterDuration}
import com.twitter.util.{Return, Throw, Future => TwitterFuture, Promise => TwitterPromise}
import scala.concurrent.{ExecutionContext, Future => ScalaFuture, Promise => ScalaPromise}
import scala.util.{Failure, Success}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{Duration => ScalaDuration}
import scala.util.{Failure, Success}

object TwitterConversions {

  // $COVERAGE-OFF$

  /** Convert from a Twitter Future to a Scala Future */
  implicit class RichTwitterFuture[A](private val tf: TwitterFuture[A]) extends AnyVal {
    def asScala: ScalaFuture[A] = {
      val promise: ScalaPromise[A] = ScalaPromise()
      tf.respond {
        case Return(value)    => promise.success(value)
        case Throw(exception) => promise.failure(exception)
      }
      promise.future
    }
  }

  // Disabling highlighting by default until a workaround for https://issues.scala-lang.org/browse/SI-8596 is found
  /** Convert from a Scala Future to a Twitter Future */
  implicit class RichScalaFuture[A](private val sf: ScalaFuture[A]) extends AnyVal {
    def asTwitter(implicit e: ExecutionContext): TwitterFuture[A] = {
      val promise: TwitterPromise[A] = new TwitterPromise[A]()
      sf.onComplete {
        case Success(value)     => promise.setValue(value)
        case Failure(exception) => promise.setException(exception)
      }
      promise
    }
  }

  implicit class RichTwitterDuration(val td: TwitterDuration) {
    def asScala: ScalaDuration = {
      ScalaDuration(td.inMicroseconds, MILLISECONDS)
    }
  }
  implicit class RichScalaDuration(val sd: ScalaDuration) {
    def asTwitter: TwitterDuration = {
      TwitterDuration(sd.toMillis, MILLISECONDS)
    }
  }
  // $COVERAGE-ON$

}
