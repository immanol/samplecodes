package com.netNative.accessprofiles.sync.logging

import java.nio.charset.StandardCharsets.UTF_8

import org.slf4j.{Marker, Logger => SLF4JLogger}

import scala.util.{Failure, Success, Try}

abstract class Encrypter {
  def encrypt(log: String): String
  def decrypt(input: String): String
}

class LogEncrypter(key: String) extends Encrypter {

  def encrypt(log: String): String = {
    if (log.isEmpty) "*** empty ***"
    else {
      Try {
        EncryptionUtil.encryptAndEncodeWithPrependedIV(log, key.getBytes(UTF_8), true)
      } match {
        case Success(b) => b
        case Failure(e) => (FailedToEncrypt(s"Log statement") causedBy e).message
      }
    }
  }

  def decrypt(input: String): String = {
    Try {
      EncryptionUtil.decodeAndDecryptWithPrependedIV(input, key.getBytes(UTF_8), true)
    } match {
      case Success(decrypted) => decrypted
      case Failure(e)         => (FailedToDecrypt("String") causedBy e).message
    }
  }
}

class Echoer extends Encrypter {

  def encrypt(log: String): String = log

  def decrypt(input: String): String = input
}

object LogEncrypter {
  def apply(key: String) = new LogEncrypter(key)
}

object Echoer {
  def apply = new Echoer()
}

/** Scala front-end to a SLF4J logger.
  */
final case class HeimdallLogger private (logslfj: SLF4JLogger, encrypter: Encrypter) {

  /** Get the name associated with this logger.
    *
    * @return the name.
    */
  protected def loggerName = logslfj.getName()

  /** Determine whether trace logging is enabled.
    */
  protected def isTraceEnabled = logslfj.isTraceEnabled()

  /** Issue a trace logging message.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    */
  @inline final def trace(msg: String): Unit =
    logslfj.trace(encrypter.encrypt(msg))

  /** Issue a trace logging message, with an exception.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def trace(msg: String, t: => Throwable): Unit =
    logslfj.trace(encrypter.encrypt(msg), t)

  /** Issue a trace logging message, with a marker and an exception.
    *
    * @param mkr  the slf4j marker object.
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def trace(mkr: Marker, msg: String, t: => Throwable): Unit =
    logslfj.trace(mkr, encrypter.encrypt(msg), t)

  /** Determine whether debug logging is enabled.
    */
  @inline final def isDebugEnabled = logslfj.isDebugEnabled

  /** Issue a debug logging message.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    */
  @inline final def debug(msg: String): Unit =
    logslfj.debug(encrypter.encrypt(msg))

  /** Issue a debug logging message, with an exception.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def debug(msg: String, t: => Throwable): Unit =
    logslfj.debug(encrypter.encrypt(msg), t)

  /** Issue a debug logging message, with a marker and an exception.
    *
    * @param mkr  the slf4j marker object.
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def debug(mkr: Marker, msg: String, t: => Throwable): Unit =
    logslfj.debug(mkr, encrypter.encrypt(msg), t)

  /** Determine whether trace logging is enabled.
    */
  @inline final def isErrorEnabled = logslfj.isErrorEnabled

  /** Issue a trace logging message.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    */
  @inline final def error(msg: String): Unit =
    logslfj.error(encrypter.encrypt(msg))

  @inline final def error(m: String, e: Exception): Unit =
    logslfj.error(encrypter.encrypt(m), e)

  /** Issue a trace logging message, with an exception.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def error(msg: String, t: => Throwable): Unit =
    logslfj.error(encrypter.encrypt(msg), t)

  /** Issue a error logging message, with a marker and an exception.
    *
    * @param mkr  the slf4j marker object.
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def error(mkr: Marker, msg: String, t: => Throwable): Unit =
    logslfj.error(mkr, encrypter.encrypt(msg), t)

  /** Determine whether trace logging is enabled.
    */
  @inline final def isInfoEnabled = logslfj.isInfoEnabled

  /** Issue a trace logging message.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    */
  @inline final def info(msg: String): Unit =
    logslfj.info(encrypter.encrypt(msg))

  /** Issue a trace logging message, with an exception.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def info(msg: String, t: => Throwable): Unit =
    logslfj.info(encrypter.encrypt(msg), t)

  /** Issue a info logging message, with a marker and an exception.
    *
    * @param mkr  the slf4j marker object.
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def info(mkr: Marker, msg: String, t: => Throwable): Unit =
    logslfj.info(mkr, encrypter.encrypt(msg), t)

  /** Determine whether trace logging is enabled.
    */
  @inline final def isWarnEnabled = logslfj.isWarnEnabled

  /** Issue a trace logging message.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    */
  @inline final def warn(msg: String): Unit =
    logslfj.warn(encrypter.encrypt(msg))

  /** Issue a trace logging message, with an exception.
    *
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def warn(msg: String, t: => Throwable): Unit =
    logslfj.warn(encrypter.encrypt(msg), t)

  /** Issue a warn logging message, with a marker and an exception.
    *
    * @param mkr  the slf4j marker object.
    * @param msg  the message object. `toString()` is called to convert it
    *             to a loggable string.
    * @param t    the exception to include with the logged message.
    */
  @inline final def warn(mkr: Marker, msg: String, t: => Throwable): Unit =
    logslfj.warn(mkr, encrypter.encrypt(msg), t)

  @inline final def fatal(msg: String): Nothing = {
    logslfj.error(encrypter.encrypt(msg))
    sys.error(msg)
  }
}

/** A factory for retrieving an SLF4JLogger.
  */
object HeimdallLogger {
  import scala.reflect.{ClassTag, classTag}

  /** The name associated with the root logger.
    */
  val RootLoggerName = SLF4JLogger.ROOT_LOGGER_NAME

  /** Get the logger with the specified name. Use `RootName` to get the
    * root logger.
    *
    * @param name  the logger name
    *
    * @return the `Logger`.
    */
  def apply(name: String)(implicit encrypter: Encrypter): HeimdallLogger =
    new HeimdallLogger(org.slf4j.LoggerFactory.getLogger(name), encrypter)

  /** Get the logger for the specified class, using the class's fully
    * qualified name as the logger name.
    *
    * @param cls  the class
    *
    * @return the `Logger`.
    */
  def apply(cls: Class[_])(implicit encrypter: Encrypter): HeimdallLogger =
    apply(cls.getName)(encrypter)

  /** Get the logger for the specified class type, using the class's fully
    * qualified name as the logger name.
    *
    * @return the `Logger`.
    */
  def apply[C: ClassTag]()(implicit encrypter: Encrypter): HeimdallLogger =
    apply(classTag[C].runtimeClass.getName)(encrypter)

  /** Get the root logger.
    *
    * @return the root logger
    */
  def rootLogger(implicit encrypter: Encrypter) = apply(RootLoggerName)(encrypter)
}

/** Mix the `Logging` trait into a class to get:
  *
  * - Logging methods
  * - A `Logger` object, accessible via the `log` property
  *
  * Does not affect the public API of the class mixing it in.
  */
trait EncryptedLogger {

  implicit val encrypter: Encrypter = LogEncrypter(ClientsConfig.logEncryptionKey)

  // The logger. Instantiated the first time it's used.
  @transient private lazy val _logger: HeimdallLogger = HeimdallLogger(getClass)

  /** Get the `Logger` for the class that mixes this trait in. The `Logger`
    * is created the first time this method is call. The other methods (e.g.,
    * `error`, `info`, etc.) call this method to get the logger.
    *
    * @return the `Logger`
    */
  protected lazy implicit val log: HeimdallLogger = _logger

}

trait UnsafeLogger {

  implicit val noencrypter: Encrypter = Echoer.apply

  // The logger. Instantiated the first time it's used.
  @transient private lazy val _logger: HeimdallLogger = HeimdallLogger(getClass)

  /** Get the `Logger` for the class that mixes this trait in. The `Logger`
    * is created the first time this method is call. The other methods (e.g.,
    * `error`, `info`, etc.) call this method to get the logger.
    *
    * @return the `Logger`
    */
  protected lazy implicit val log: HeimdallLogger = _logger

}
