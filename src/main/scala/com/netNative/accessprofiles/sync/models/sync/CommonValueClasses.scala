package com.netNative.accessprofiles.sync.models.sync

object CommonValueClasses {
  case class ClientId(id: String)     extends AnyVal
  case class GroupId(id: String)      extends AnyVal
  case class TopicName(value: String) extends AnyVal
}

sealed trait FileSourceTypes
object FileSourceTypes {

  case object Party         extends FileSourceTypes
  case object PartyWithType extends FileSourceTypes

  case object Agreement extends FileSourceTypes

  case object Profile extends FileSourceTypes

}
