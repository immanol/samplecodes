package com.netNative.accessprofiles.sync.models

import akka.kafka.ConsumerMessage
import com.netNative.errors.ErrorMessage
import io.opentracing.Span

trait DataContext {
  def message: Option[ConsumerMessage.CommittableMessage[String, Eventdata]]
  def data: DataContainer
  def result: Either[AppError, Seq[ProcessResult]]

  def headerWriter: String = {
    val hm = data.header
    (for {
      cid         <- hm.get("ServiceEvent_CorrelationId")
      pid         <- hm.get("ServiceEvent_ParentEventId")
      ldd         <- hm.get("AccessTokenValue")
      time        <- hm.get("NotificationTime")
      transaction <- hm.get("TransactionType")
      spanId      <- hm.get("SpanId")
    } yield s"$cid | $pid | $ldd | $time | $transaction | $spanId")
      .getOrElse(s"Error in headerMap $hm")
  }

  def dataWriter: String =
    data.messageIds.map(_.map(_.value).mkString("|")).getOrElse("There is no data available")
}

final case class DataContextInstance(
    message: Option[ConsumerMessage.CommittableMessage[String, Eventdata]],
    data: DataContainer,
    result: Either[AppError, Seq[ProcessResult]])
    extends DataContext {

  val netspan: INGSpanContext = Tracing.getParentSpanFromMessage(data.header)
  val span: Span = message match {
    case Some(m) =>
      Tracing.setSpanCommitOffset(
        Tracing.startSpan(Tracing.operationName(data.header), netspan, Tracing.tracer),
        m)
    case None => Tracing.startSpan(Tracing.operationName(data.header), netspan, Tracing.tracer)
  }

  def logSpanId(): String = {
    val spanId =
      data.header.getOrElse("SpanId", "Error in headerMap and can not get correlationId")
    s"$spanId,${data.messageType}"
  }

  def updateResult(res: Either[AppError, Seq[ProcessResult]]): DataContextInstance =
    this.copy(result = res)
  def updateError(error: AppError): DataContextInstance =
    this.copy(result = Left(error))
  def updateData(d: Either[AppError, Seq[TPAIdentifier]]): DataContextInstance = {
    d match {
      case Right(_) => this.copy(data = data.copy(messageIds = d, retryCount = 1, errorCode = None))
      case Left(e) =>
        this.copy(data = data.copy(messageIds = d,
                                   retryCount = this.data.retryCount + 1,
                                   errorCode = Some(ErrorMessage(e.errorInfo).errorCode)),
                  result = Left(e))
    }
  }
  def updateFailedData(res: Either[AppError, Seq[ProcessResult]]): DataContextInstance = {
    res match {
      case Right(_) => this.copy(data = data.copy(retryCount = 1, errorCode = None))
      case Left(e) =>
        this.copy(data = data.copy(retryCount = this.data.retryCount + 1,
                                   errorCode = Some(ErrorMessage(e.errorInfo).errorCode)),
                  result = Left(e))
    }
  }

}
