package com.netNative.accessprofiles.sync.models

import com.netNative.accessprofiles.sync.models.sync.CommonValueClasses.TopicName
import org.apache.avro.Schema
import org.apache.avro.specific.SpecificRecordBase

trait Topic[T <: SpecificRecordBase] {

  def clazz: T

  def schema: Schema = clazz.getSchema

  def name: TopicName

  def sharedSecret: String

  def schemaVersion: String

  def consumerClientId: String

  def consumerGroupId: String

  def autoOffsetReset: String

  def messageType: Seq[String]

  def headerMap(t: T): Map[String, String]

}
