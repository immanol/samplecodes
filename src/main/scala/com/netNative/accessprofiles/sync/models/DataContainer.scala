package com.netNative.accessprofiles.sync.models

import com.netNative.accessprofiles.sync.utils.EncodersDecoders.KafkaNotificationDateDecoders

case class DataContainer(header: Map[String, String],
                         messageIds: Either[AppError, Seq[TPAIdentifier]],
                         messageType: String,
                         topicName: String,
                         retryCount: Int = 0,
                         errorCode: Option[String] = None) {

  header
    .get("NotificationTime")
    .filterNot(_.trim.isEmpty)
    .foreach(d => updateTimeMetrics(d))

  private def updateTimeMetrics(dateStr: String): Unit = {

    val diffInMilli =
      ProfileSyncTimeFormat.timeDiffInMilli(dateStr)(KafkaNotificationDateDecoders.formatter)
    //Sometimes notifications has time that is in future!
    Metrics.Latency.record(diffInMilli.max(0), "topic" -> topicName)
  }

}
