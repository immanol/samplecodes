package com.netNative.accessprofiles.sync.consumers

import akka.kafka.ConsumerSettings
import com.netNative.accessprofiles.sync.models.Topic
import com.typesafe.config.Config
import org.apache.avro.specific.SpecificRecordBase
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

object AvroConsumerSettingsFactory {

  def consumerSettings[T <: SpecificRecordBase](
      config: Config,
      topic: Topic[T]): ConsumerSettings[String, Eventdata] = {

    val ingDecryptingDeserializer =
      KafkaSerDeFactory.decryptingDeserializer(topic.sharedSecret, topic.schemaVersion)
    //This will read the akka.kafka.consumer configs
    val kafkaCfg = config.getConfig("akka.kafka.consumer")

    val keyDeserializer = new StringDeserializer

    ConsumerSettings(kafkaCfg, keyDeserializer, ingDecryptingDeserializer)
      .withBootstrapServers(ClientsConfig.appConfig.bootstrapServers)
      .withGroupId(topic.consumerGroupId)
      .withClientId(topic.consumerClientId)
      .withProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, keyDeserializer.getClass.getName)
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, topic.autoOffsetReset)
  }

}
