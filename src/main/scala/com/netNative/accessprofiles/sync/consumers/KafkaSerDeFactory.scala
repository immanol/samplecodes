package com.netNative.accessprofiles.sync.consumers

import scala.collection.JavaConverters._

object KafkaSerDeFactory {

  def decryptingDeserializer(sharedSecret: String, schemaVersion: String): KafkaByteDeserializer = {
    val deserializer: KafkaByteDeserializer = new KafkaByteDeserializer
    deserializer.configure(serdesCongfig(sharedSecret, schemaVersion), false)
    deserializer
  }

  def encryptingSerializer(sharedSecret: String, schemaVersion: String): KafkaByteSerializer = {
    val serializer: KafkaByteSerializer = new KafkaByteSerializer
    serializer.configure(serdesCongfig(sharedSecret, schemaVersion), false)
    serializer
  }

  private def serdesCongfig(sharedSecret: String, schemaVersion: String) = {
    Map(
      KafkaSerdesConfig.BYTE_SCHEMA_VERSION_CONFIG  -> schemaVersion,
      KafkaSerdesConfig.BYTE_C3_SHAREDSECRET_CONFIG -> sharedSecret
    ).asJava
  }
}
