package com.netNative.accessprofiles.sync.consumers

import java.time.ZonedDateTime
import java.util.UUID

import com.netNative.accessprofiles.sync.models.sync.FileSourceTypes
import com.netNative.accessprofiles.sync.models.{DataContainer, DataContextInstance}

import scala.util.{Failure, Success, Try}
import com.netNative.accessprofiles.sync.utils.EncodersDecoders.KafkaNotificationDateDecoders
import com.netNative.errors.{AppException, FailedToParseUUID}

object FileConsumer {

  def decodeFileData(line: String,
                     fileName: String,
                     lineNumber: Long,
                     fileSourceType: FileSourceTypes): DataContextInstance =
    DataContextInstance(
      message = None,
      data = tryDecode(line, fileName, lineNumber, fileSourceType),
      result = Right(Seq.empty)
    )

  private def tryDecode(line: String,
                        fileName: String,
                        lineNumber: Long,
                        fileSourceTypes: FileSourceTypes): DataContainer = {

    val formattedNow = ZonedDateTime.now().format(KafkaNotificationDateDecoders.formatter)

    val headers =
      (identifier: String) =>
        Map(
          "ServiceEvent_CorrelationId"   -> UUID.randomUUID().toString,
          "SpanId"                       -> s"$fileName:$lineNumber:$identifier",
          "ServiceEvent_ParentEventId"   -> UUID.randomUUID().toString,
          "AccessTokenValue"             -> "ING_NL",
          "TransactionType"              -> "File_Load",
          "NotificationTime"             -> formattedNow,
          "NotificationReceivedDateTime" -> formattedNow
      )

    val (tpsIdentifier, msgType) = getId(line, fileSourceTypes)

    tpsIdentifier match {
      case Success(id) =>
        DataContainer(
          header = headers(id.value.toString),
          messageIds = Right(Seq(id)),
          messageType = msgType,
          topicName = "File"
        )

      case Failure(_) =>
        DataContainer(
          header = headers(line),
          messageIds = Left(
            FailedToParseUUID(s"Can not parse $line as a AgreementUUID in $fileName:$lineNumber")),
          messageType = msgType,
          topicName = "File"
        )
    }

  }

  private val defaultLineDecoder = (line: String, fileSourceType: FileSourceTypes) =>
    (Try(UUID.fromString(line)), fileSourceType.getClass.getSimpleName)

  private val commaSeparatedLineDecoder = (line: String) => {
    val pattern =
      "([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}),([1-2]{1})".r
    line match {
      case pattern(uuid, personType) =>
        (Try(UUID.fromString(uuid)), personType)
      case _ =>
        (Failure(
           AppException(
             FailedToParseUUID(
               s"It can not match the pattern :${pattern.toString()} with string: ${line}"))),
         "PPG-Init-file")
    }

  }
  private def getId(line: String, fileSourceType: FileSourceTypes): (Try[TPAIdentifier], String) =
    fileSourceType match {
      case FileSourceTypes.Party =>
        val (uuid: Try[UUID], msgType: String) =
          defaultLineDecoder(line, fileSourceType: FileSourceTypes)
        (uuid.map(id => PartyUUID(id)), msgType)

      case FileSourceTypes.Agreement =>
        val (uuid: Try[UUID], msgType: String) =
          defaultLineDecoder(line, fileSourceType: FileSourceTypes)
        (uuid.map(id => AgreementUUID(id)), msgType)

      case FileSourceTypes.Profile =>
        val (uuid: Try[UUID], msgType: String) =
          defaultLineDecoder(line, fileSourceType: FileSourceTypes)
        (uuid.map(id => ProfileUUID(id)), msgType)

      case FileSourceTypes.PartyWithType =>
        val (uuid: Try[UUID], msgType: String) = commaSeparatedLineDecoder(line)
        (uuid.map(id => PartyUUID(id)), msgType)

    }

}
