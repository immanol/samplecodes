package com.netNative.accessprofiles.sync.consumers

import akka.Done
import akka.stream.UniqueKillSwitch

import scala.concurrent.Future

trait GenericConsumer {
  def run(): (UniqueKillSwitch, Future[Done])
}
