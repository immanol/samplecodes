package com.netNative.accessprofiles.sync.flows

import akka.stream._
import akka.stream.stage._
import com.netNative.accessprofiles.sync.models.DataContextInstance

abstract class DataFlow extends GraphStage[FlowShape[DataContextInstance, DataContextInstance]] {

  val flowName: String
  final val in: Inlet[DataContextInstance]   = Inlet.create(flowName + ".in")
  final val out: Outlet[DataContextInstance] = Outlet.create(flowName + ".out")

  def update(stream: DataContextInstance): DataContextInstance

  override def shape: FlowShape[DataContextInstance, DataContextInstance] =
    FlowShape.of(in, out)

  override def createLogic(attr: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          push(out, update(grab(in)))
        }
      })
      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          pull(in)
        }
      })
    }

}
