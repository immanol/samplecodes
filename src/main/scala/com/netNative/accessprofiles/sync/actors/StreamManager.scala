package com.netNative.accessprofiles.sync.actors

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import akka.pattern.{Backoff, BackoffSupervisor}
import akka.stream.{ActorMaterializer, IOResult, UniqueKillSwitch}
import akka.stream.scaladsl.RunnableGraph
import com.netNative.accessprofiles.sync.actors.StreamMonitor.{StartStream, StopStream}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.{Duration, FiniteDuration}

case class StreamManager(
    graph: RunnableGraph[((Future[IOResult], Future[IOResult]), UniqueKillSwitch)],
    streamOn: String)(implicit materializer: ActorMaterializer, ec: ExecutionContext) {

  /**
    * Streams Actor System.
    */
  private val actorSystem = ActorSystem("SyncStreamsProfiles")

  private lazy val streamMonitorProps: Props = StreamMonitor.props(
    graph,
    streamOn
  )
  private val supervisorProps: Props = BackoffSupervisor.props(
    Backoff
      .onFailure(streamMonitorProps,
                 s"streamMonitor-${streamOn}",
                 Duration.create(3, TimeUnit.SECONDS),
                 Duration.create(30, TimeUnit.SECONDS),
                 0.2)
      .withAutoReset(FiniteDuration.apply(10, TimeUnit.SECONDS))
  )

  private def createActor =
    actorSystem.actorOf(supervisorProps, s"streamMonitor-supervisor-${streamOn}")

  private val actor = createActor

  def startSync: Unit = actor ! StartStream(streamOn)
  def stopSync: Unit  = actor ! StopStream(streamOn)

}
