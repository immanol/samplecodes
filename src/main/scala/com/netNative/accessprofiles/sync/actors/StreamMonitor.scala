package com.netNative.accessprofiles.sync.actors

import akka.actor.{Actor, ActorLogging, PoisonPill, Props}
import akka.stream._
import akka.stream.scaladsl.RunnableGraph
import com.netNative.accessprofiles.sync.actors.StreamMonitor.{KillStream, StartStream, StopStream}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class StreamMonitor(
    graph: RunnableGraph[((Future[IOResult], Future[IOResult]), UniqueKillSwitch)],
    streamOn: String)(implicit materializer: ActorMaterializer, ec: ExecutionContext)
    extends Actor
    with ActorLogging {

  override def preStart() = {

    //check SchemaRegistry if topic is available
    log.info(s"Starting StreamMonitor for ${streamOn}")
  }

  override def preRestart(reason: Throwable, message: Option[Any]) = {
    log.error(reason,
              "Restarting due to [{}] when processing [{}]",
              reason.getMessage,
              message.getOrElse(""))

  }
  override def postStop(): Unit = {
    log.info(s"Stopped StreamMonitor for ${streamOn}")
  }

  val callBackForFileSink: String => Try[(IOResult, IOResult)] => Unit =
    (topic: String) => {
      case scala.util.Failure(e) => {
        log.info(s"There is an exception on File Sink: ${e.getMessage}")
        self ! KillStream(topic, e)
      }
      case scala.util.Success(value) => {
        log.info(s"Graph stop with success message:${value}")
        self ! StopStream(topic)
      }
    }
  var switch: UniqueKillSwitch = null

  def receive: Receive = {
    case StartStream(topic) => {
      log.info(s"Starting Stream for ${topic}")

      val ((s1, s2), sw) = graph.run()
      switch = sw
      s1.zip(s2).onComplete(r => callBackForFileSink(topic)(r))

      log.info(s"Started Stream for ${topic}")
    }

    case StopStream(topic) =>
      log.info(s"Stopping Stream for ${topic}")
      if (switch != null) switch.shutdown()

    case KillStream(topic, ex) =>
      log.info(s"Killing Stream for ${topic} due to ${ex.getMessage}")
      if (switch != null) switch.abort(ex)
      self ! PoisonPill
  }

}

object StreamMonitor {

  sealed trait StreamCommands

  final case class StartStream(topic: String)               extends StreamCommands
  final case class StopStream(topic: String)                extends StreamCommands
  final case class KillStream(topic: String, ex: Throwable) extends StreamCommands

  def props(graph: RunnableGraph[((Future[IOResult], Future[IOResult]), UniqueKillSwitch)],
            streamOn: String)(implicit materializer: ActorMaterializer, ec: ExecutionContext) =
    Props(new StreamMonitor(graph, streamOn))
}
