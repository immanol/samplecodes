package com.netNative.accessprofiles.app

object ProfileManagerService extends App {

  val application = new ApplicationDaemon

  private[this] var cleanupAlreadyRun: Boolean = false

  def cleanup(): Unit = {
    val previouslyRun = cleanupAlreadyRun
    cleanupAlreadyRun = true
    if (!previouslyRun) application.stop()
  }

  Runtime.getRuntime.addShutdownHook(new Thread(() => cleanup()))

  application.start()
}
