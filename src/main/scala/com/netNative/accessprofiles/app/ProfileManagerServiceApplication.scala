package com.netNative.accessprofiles.app

import akka.actor.CoordinatedShutdown.JvmExitReason
import akka.actor._
import akka.stream.Supervision.Decider
import akka.stream.scaladsl.RunnableGraph
import akka.stream._
import com.netNative.accessprofiles.business._
import com.netNative.accessprofiles.business.streams.{
  GraphOps
}
import com.netNative.accessprofiles.sync.actors.StreamManager
import com.twitter.finagle.Service
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finagle.loadbalancer.LoadBalancerFactory
import com.twitter.finagle.service.RetryFilter

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.Future

class ProfileManagerServiceApplication() extends ApplicationLifecycle with UnsafeLogger {

  import ProfileManagerServiceApplication._

  //TODO catch fatal exceptionsx
  private val decider: Decider = { e =>
    {
      log.error("Catched by decider:", e)
      Supervision.Resume
    }
  }
  implicit val actorSystem: ActorSystem = ActorSystem(s"accessprofile-sync-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer(
    ActorMaterializerSettings(actorSystem).withSupervisionStrategy(decider))
  implicit val ec: ExecutionContextExecutor = actorSystem.dispatcher

  /*
   * API Services
   */
  private val pAPI = getPService()

  private val mAPI = ???

  private val iAPI = ???

  private val aAPI = ???

  /*
   * Business Flows
   */
  val a2aFlow = new A2ABiz(pAPI, mAPI, iAPI, aAPI)


  private lazy val dedicatedManager = {

    val graph = GraphOps(a2aFlow).runGraph(ClientsConfig.config)

    getStreamManager(graph, "Dedicated-AccessProfile-Graph")
  }

  def getStreamManager(
      graph: RunnableGraph[((Future[IOResult], Future[IOResult]), UniqueKillSwitch)],
      name: String) =
    StreamManager(graph, name)

  override def start(): Unit = {
    if (!started) {
      if (ClientsConfig.appConfig.graphSettings.isDedicatedGraphEnabled)
        dedicatedManager.startSync
      started = true
    }
  }

  override def stop(): Unit = {

    log.info("Mayday Mayday Mayday this is ProfileSync going down ........... ")
    if (started) {
      if (ClientsConfig.appConfig.graphSettings.isDedicatedGraphEnabled)
        dedicatedManager.stopSync
      started = false
      CoordinatedShutdown(actorSystem).run(JvmExitReason)
    }
  }

}

object ProfileManagerServiceApplication extends UnsafeLogger {
  private[ProfileManagerServiceApplication] var started: Boolean = false

  implicit val loadBalancerFactory: LoadBalancerFactory =
    BalancerSelector(loadBalancerClientConfig)

  def getPService() = {

    val pAPIRetryFilter: RetryFilter[Request, Response] = FinagleClientFactory
      .createRetryFilter(pClientConfig, retryBudgetConfig, backoffConfig)

    val prEndpoint: PEndpoint =
      PEndpoint(
        getEndpoint(pClientConfig, "prById"))

    val pByIdEndpoint: PByIdEndpoint =
      PByIdEndpoint(
        getEndpoint(pClientConfig, "pById"))

    val pByIdAPIService: Service[Request, Response] = validateClient(
      FinagleClientFactory.createClientService(pClientConfig,
                                               tracingConfig,
                                               pAPIRetryFilter,
                                               pByIdEndpoint.value,
                                               Some(sSlContext)))

    val prAPIService: Service[Request, Response] = validateClient(
      FinagleClientFactory.createClientService(pClientConfig,
                                               tracingConfig,
                                               pAPIRetryFilter,
                                               prEndpoint.value,
                                               Some(sSlContext)))

    PServiceImpl(
      pByIdAPIService,
      prAPIService,
      ClientsConfig.pClientConfig,
      prEndpoint,
      pByIdEndpoint
    )
  }

}
